import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
//import RemoteAssets from 'vite-plugin-remote-assets'
import path from "path";

export default defineConfig(() => {
  return {
    plugins: [vue()],
    base: "/html/",
    resolve: {
      alias: { "@": path.resolve(__dirname, "/src") },
    },
    server: {
      host: "0.0.0.0",
      port: 3030,
      hmr: {
        protocol: "wss",
        port: 443,
        path: "",
      },
    },
    define: {
      "process.env": {},
    },
    build: {
      sourcemap: true,
    },
  };
});
