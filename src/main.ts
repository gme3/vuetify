import { createApp } from 'vue'
import { bootstrap } from './bootstrap'
import BootstrapError from './views/errors/BootstrapError.vue'
import App from './App.vue'

import './main.scss'

const MOUNTPOINT = '#app'

const app = createApp(App)
bootstrap(app)
	.then(() => {
		app.mount(MOUNTPOINT)
	})
	.catch(e => {
		// Give user some clue with UI
		console.log('Failed to initialize')
		console.log(e)
		createApp(BootstrapError).mount(MOUNTPOINT)
	})
