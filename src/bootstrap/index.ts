import { App } from 'vue'
import * as vendors from './vendors'

async function bootstrap(
	app: App,
	options: { prev?: () => Promise<void>; after?: () => Promise<void> } = {},
): Promise<void> {
	if (options.prev) {
		await options.prev()
    }

    const plugins = [vendors.Vuetify]
	plugins.forEach((item) => app.use(item))

    if (options.after) {
		await options.after()
	}
}

export { bootstrap }